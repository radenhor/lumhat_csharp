﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lumhat.Model
{
    class Answer
    {
        private int id;
        private String option;
        private bool correct;

        public Answer()
        {
        }

        public Answer(int id, String option, bool correct)
        {
            this.id = id;
            this.option = option;
            this.correct = correct;
           
        }

        public int getId()
        {
            return id;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public String getOption()
        {
            return option;
        }

        public void setOption(String option)
        {
            this.option = option;
        }

        
        public bool getCorrect()
        {
            return this.correct;
        }
        public void setCorrect(bool correct)
        {
            this.correct = correct;
        }
    }
}
