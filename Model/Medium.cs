﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Model;

namespace Lumhat.Model
{
    class Medium : ILevel
    {
        public QuizShow DoQuiz(QuizShow quiz)
        {
            quiz.setDuration(quiz.getDuration() - 2);
            return quiz;
        }
    }
}
