﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Model
{
    public class QuizShow
    {
        private int id;
        private String title;
        private int duration;
        public QuizShow(int id, String title)
        {
            this.id = id;
            this.title = title;
        }

        public QuizShow(int id, String title,int duration)
        {
            this.id = id;
            this.duration = duration;
            this.title = title;
        }

        public QuizShow() { }

        public int getId()
        {
            return id;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public String getTitle()
        {
            return title;
        }

        public void setTitle(String title)
        {
            this.title = title;
        }

        public int getDuration()
        {
            return this.duration;
        }

        public void setDuration(int duration)
        {
            this.duration = duration;
        }
    }
}

