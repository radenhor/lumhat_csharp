﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lumhat.Model
{
    class Instruction
    {
        private int id;
        private String title;

        public Instruction()
        {
        }

        public Instruction(int id, String title)
        {
            this.id = id;
            this.title = title;
        }

        public int getId()
        {
            return id;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public String getTitle()
        {
            return title;
        }

        public void setTitle(String title)
        {
            this.title = title;
        }

    }
}
