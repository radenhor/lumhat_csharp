﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Model
{
    class Subject
    {
        private int id;
        private string subject;

        public Subject() { }

        public Subject(int id, string subject)
        {
            this.id = id;
            this.subject = subject;
        }

        public int getId() { return this.id; }

        public void setId(int id) { this.id = id; }

        public String getSubject() { return this.subject; }

        public void setSubject(String subject) { this.subject = subject; }
    }
}
