﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Model
{
    class SubMajor
    {
        private int id;
        private string subMajor;

        public SubMajor() { }

        public SubMajor(int id,string subMajor)
        {
            this.id = id;
            this.subMajor = subMajor;
        }

        public int getId() { return this.id; }

        public void setId(int id) { this.id = id; }

        public String getSubMajor() { return this.subMajor; }

        public void setSubMajor(String subMajor) { this.subMajor = subMajor; }
    }
}
