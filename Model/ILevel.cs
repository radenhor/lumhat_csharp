﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Model;

namespace Lumhat.Model
{
    interface ILevel
    {
        QuizShow DoQuiz(QuizShow quiz);
    }
}
