﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Npgsql;
using System.Threading.Tasks;

namespace Lumhat.Configuration
{
    public class DatabaseConnection
    {
        private static DatabaseConnection _instance = null;
        private static NpgsqlConnection con = null;
        static object _syncObject = new object();
        private DatabaseConnection()
        {
            string conString = "Server=52.77.227.48; Port=5432; User Id=rabbit; Password=rabbit+_); Database=lumhat_db";
            try
            {
                con = new NpgsqlConnection(conString);
                con.Open();

            }

            catch (Exception e)
            {
                con.Close();
                Console.Write(e.Message);
            }
        }

        public static DatabaseConnection Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncObject)
                    {
                        if (_instance == null)
                        {
                            _instance = new DatabaseConnection();
                        }
                    }
                }
                return _instance;
            }
        }
        public NpgsqlConnection GetDBConnection()
        {
            return con;
        }
    }
}
