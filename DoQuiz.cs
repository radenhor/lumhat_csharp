﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Utility;
using Lumhat.Model;
using WindowsFormsApp1.Model;
using WindowsFormsApp1.Service;
using Lumhat.Utility;


namespace WindowsFormsApp1
{
    public partial class DoQuiz : Form
    {
        int qCount = 0,score = 0;
        List<Question> allQuestion;
        List<Answer> allAnswer;
        List<TableLayoutPanel> allCard;
        HashSet<Int32> clickedAns = new HashSet<int>();
        List<RadioButton> radioButtons;
        public DoQuiz()
        {
            InitializeComponent();
        }

        private void DoQuiz_Load(object sender, EventArgs e)
        {
            
        }

        public void countDownTime(int minute)
        {
            CountDownTimer timer = new CountDownTimer();


            //set to time mins
            timer.SetTime(minute, 0);

            timer.Start();

            //update label text
            timer.TimeChanged += () => lblDuration.Text = "Time : "+ timer.TimeLeftMsStr;

            // show messageBox on timer = 00:00.000
            timer.CountDownFinished += () => {
                if (MessageBox.Show("Out of time?", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    btnDone.PerformClick();
                }
                else
                {
                    btnDone.PerformClick();
                }
            };

            //timer step. By default is 1 second
            timer.StepMs = 33;
        }
        

        public void renderQuiz(int quizId,int duration)
        {
            score = 0;
            allAnswer = new List<Answer>();
            allQuestion = new List<Question>();
            radioButtons = new List<RadioButton>();
            allCard = new List<TableLayoutPanel>();
            countDownTime(duration);
            QuizService quizService = new QuizService();   
            List<Instruction> instructions = quizService.findAllInstructionByQuiz(quizId);
            foreach (Instruction instruction in instructions)
            {
                List<Question> questions = quizService.findAllQuestionByInstruction(instruction.getId());
                allQuestion.AddRange(questions);
            }
            
            lblQuestionCount.Text = "Question 0 /" + allQuestion.Count;
            TableLayoutPanel quiz = new TableLayoutPanel();
            quiz.ColumnCount = 1;
            quiz.RowCount = allQuestion.Count;
            int i = 1;
            foreach (Question question in allQuestion)
            {
                //Add question
                TableLayoutPanel q = new TableLayoutPanel();
                q.Dock = DockStyle.Fill;
                q.RowCount = 5;
                q.ColumnCount = 1;
                Label lblQuestion = new Label();
                lblQuestion.Text = i + " ) " + question.getTitle();
                lblQuestion.Font = new Font("Arial", 14, FontStyle.Regular);
                lblQuestion.AutoSize = true;
                q.Controls.Add(lblQuestion);
                //Add answer
                List<Answer> answers = quizService.findAllAnswerByQuestion(question.getId());
                allAnswer.AddRange(answers);
                foreach (Answer answer in answers)
                {
                    RadioButton radioButton = new RadioButton();
                    radioButton.Dock = DockStyle.Top;
                    radioButton.Name = ""+ answer.getCorrect();
                    radioButton.Font = new Font("Arial", 12, FontStyle.Regular);
                    radioButton.Tag = answer.getId();
                    radioButton.Click += (s, e) => { checkAnswer(allQuestion.Count, question.getId(),radioButton,answer.getId()); };
                    radioButton.TextAlign = ContentAlignment.MiddleLeft;
                    radioButton.Text = answer.getOption();
                    q.Controls.Add(radioButton);
                    radioButtons.Add(radioButton);
                }
                i++;
                q.AutoSize = true;
                quiz.AutoScroll = true;
                quiz.Dock = DockStyle.Fill;
                quiz.Controls.Add(q);
                allCard.Add(quiz);
                quizPanel.Controls.Add(quiz);
            }

        }

        public void checkAnswer(int totalQ, int currentID, RadioButton radioButton, int ansId)
        {
            bool increase = true;
            if (clickedAns.Count != 0)
            {
                foreach (int clicked in clickedAns)
                {
                    if (clicked == currentID)
                    {
                        increase = false;
                    }
                }
            }
            else
            {
                increase = true;
            }
            if (increase)
            {
                qCount++;
                lblQuestionCount.Text = "Question " + qCount + "/" + totalQ;
            }

            clickedAns.Add(currentID);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            panel.SendToBack();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            //Caculate score
            int score = 0;
            foreach (RadioButton radioButton in radioButtons)
            {
                if (radioButton.Name == "True" && radioButton.Checked)
                {
                    score++;
                }
            }
            //Show result
            showResultPanel.BringToFront();
            showAnswer(allAnswer, score);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            panel.SendToBack();
        }

        private void quizPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        void showAnswer(List<Answer> answers, int score)
        {
            
            foreach (TableLayoutPanel card in allCard)
            {
                panelAnswer.Controls.Add(card);
            }
            foreach(RadioButton radioButton in radioButtons)
            {
                if (radioButton.Name == "True")
                {
                    radioButton.BackColor = Color.Green;
                }
                else if(radioButton.Name == "False" && radioButton.Checked)
                {
                    radioButton.BackColor = Color.Red;
                }
            }
            lblScore.Text = "You got score " + score + "/" + allQuestion.Count;
        }

    }

}
