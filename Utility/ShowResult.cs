﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lumhat.Model;

namespace Lumhat.Utility
{
    public partial class ShowResult : Form
    {
        public ShowResult()
        {
            InitializeComponent();
        }

        public void showAnswer(Object allAnswer,int score)
        {
            int i = 1;
            List<Answer> answers = (List<Answer>)allAnswer;
            TableLayoutPanel tbAnswer = new TableLayoutPanel();
            tbAnswer.RowCount = answers.Count;
            tbAnswer.ColumnCount = 1;
            tbAnswer.Dock = DockStyle.Fill;
            tbAnswer.AutoScroll = true;
            foreach(Answer answer in answers)
            {
                if (answer.getCorrect())
                {
                    Label lbCorrect = new Label();
                    lbCorrect.Text = i + " ) " + answer.getOption();
                    lblScore.ForeColor = Color.White;
                    lbCorrect.Dock = DockStyle.Fill;
                    tbAnswer.Controls.Add(lbCorrect);
                    i++;
                }
            }
            panelAnswer.Controls.Add(tbAnswer);
            lblScore.Text = "you got score " + score + "/" + answers.Count;
        }
    }
}
