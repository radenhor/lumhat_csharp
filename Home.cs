﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Service;
using WindowsFormsApp1.Model;
using Lumhat.Model;

namespace WindowsFormsApp1
{
    public partial class Lumhat : Form
    {
        CategoryService categoryService;
        QuizService quizService;
        public Lumhat()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            quizService = new QuizService();
            categoryService = new CategoryService();
            
            
            catPanel.SendToBack();
            mainPanel.BringToFront();
            
            
            List<Instruction> instructions = quizService.findAllInstructionByQuiz(134);
            foreach(Instruction instruction in instructions)
            {
                List<Question> questions = quizService.findAllQuestionByInstruction(instruction.getId());
                foreach(Question question in questions)
                {
                    Console.WriteLine(question.getTitle());
                    List<Answer> answers = quizService.findAllAnswerByQuestion(question.getId());
                    foreach(Answer answer in answers)
                    {
                        Console.WriteLine(answer.getOption());
                    }
                }
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        
        private void english_Click(object sender, EventArgs e)
        {
            catPanel.BringToFront();
            mainPanel.SendToBack();
            addWelComeTab();
            findQuiz(2);
        }

        private void addWelComeTab()
        {
            tabLevel.Controls.Clear();
            TabPage tp = new TabPage("Welcome");
            tp.Font = new Font("Arial", 24, FontStyle.Bold);
            Label lbl = new Label();
            lbl.Font = new Font("Arial", 14, FontStyle.Bold);
            lbl.Text = "សូម​ធ្វើ​ការ​ជ្រើស​រើស​មុខវិជ្ជា​ណា​មួយ​ដែល​អ្នក​ចង់​ធ្វើ​តេស្ដ ​ដើម្បី​ចាប់​ផ្ដើម​សាកល្បង​ចំនេះ​ដឹង​របស់​អ្នក​ជាមួយ​លំហាត់​ឥឡូវ​នេះ។";
            lbl.Dock = DockStyle.Top;
            tp.Controls.Add(lbl);
            tabLevel.TabPages.Add(tp);
        }

        private void findQuiz(int subMajorID)
        {
            List<SubMajor> subMajors = categoryService.findSubMajorByCategory(subMajorID);
            foreach (SubMajor sub in subMajors)
            {
                subMajor.Nodes.Add("" + sub.getId(), sub.getSubMajor());
                subMajor.Font = new Font("Arial", 24, FontStyle.Bold);
                List<Subject> subjects = categoryService.findSubjectBySubMajor(sub.getId());
                foreach (Subject subject in subjects)
                {
                    if (subMajor != null)
                    {
                        subMajor.Nodes["" + sub.getId()].Nodes.Add("" + subject.getId(), subject.getSubject());
                        subMajor.Font = new Font("Arial", 20, FontStyle.Regular);
                    }
                }
                subMajor.ExpandAll();
            }
        }

        private void addTabLevel(List<Level> levels,int subID)
        {
            tabLevel.Controls.Clear();
            ILevel level1 = null;
            if (levels.Count > 0)
            {
                foreach (Level level in levels)
                {
                    TabPage tp = new TabPage(level.getName());
                    tp.AutoScroll = true;
                    tabLevel.TabPages.Add(tp);
                    List<QuizShow> quizzes = quizService.findAllQuizByLevelAndSubMajor(level.getId(), subID);
                    foreach (QuizShow quiz in quizzes)
                    {
                        Button btn = new Button();
                        btn.Dock = DockStyle.Top;
                        btn.Tag = quiz.getId();
                        btn.Click += (s, e) => {
                            if (level.getName().ToUpper().Equals("BASIC"))
                            {
                                Console.WriteLine("BASIC");
                                level1 = new Basic();
                                doQuiz(level1.DoQuiz(quiz));
                            }
                            else if (level.getName().ToUpper().Equals("MEDIUM"))
                            {
                                Console.WriteLine("MEDIUM");
                                level1 = new Medium();
                                doQuiz(level1.DoQuiz(quiz));
                            }
                            else if (level.getName().ToUpper().Equals("ADVANCED"))
                            {
                                Console.WriteLine("ADVANCED");
                                level1 = new Advance();
                                doQuiz(level1.DoQuiz(quiz));
                            }
                           
                        };
                        btn.Text = "Quiz " + quiz.getTitle();
                        tp.Controls.Add(btn);
                    }
                }
            }
            else
            {
                TabPage tp = new TabPage("404");
                Label lbl = new Label();
                lbl.Text = "This subject is comming soon";
                lbl.Dock = DockStyle.Top;
                tp.Controls.Add(lbl);
                tabLevel.TabPages.Add(tp);
            }
           
        }

        private void subMajor_Click(object sender, EventArgs e)
        {
            
        }

        private void subMajor_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Parent != null)
            {
                int id = Convert.ToInt32(e.Node.Name);
                List<Level> levels = categoryService.findLevelBySubMajorId(id);
                addTabLevel(levels, id);
            }
            
        }

        private void korean_Click(object sender, EventArgs e)
        {
            catPanel.BringToFront();
            mainPanel.SendToBack();
            addWelComeTab();
            findQuiz(3);
        }

        private void program_Click(object sender, EventArgs e)
        {
            catPanel.BringToFront();
            mainPanel.SendToBack();
            addWelComeTab();
            findQuiz(1);
        }

        public void doQuiz(QuizShow quiz)
        {
            DoQuiz doQuiz = new DoQuiz();
            foreach (Control control in doQuiz.Controls)
            {
                if (control.Name.Equals("panel"))
                {
                    Panel doQuizPanel = (Panel)control;
                    doQuizPanel.Dock = DockStyle.Fill;
                    Controls.Add(doQuizPanel);
                    doQuiz.renderQuiz(quiz.getId(),quiz.getDuration());
                    doQuizPanel.BringToFront();
                    //break;
                }
                else if (control.Name.Equals("showResultPanel"))
                {
                    Panel showResultPanel = (Panel)control;
                    showResultPanel.Dock = DockStyle.Fill;
                    Controls.Add(showResultPanel);
                    showResultPanel.SendToBack();
                    //break;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            subMajor.Nodes.Clear();
            tabLevel.Controls.Clear();
            catPanel.SendToBack();
            mainPanel.BringToFront();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
