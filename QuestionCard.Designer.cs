﻿namespace Lumhat
{
    partial class QuestionCard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.question = new System.Windows.Forms.TableLayoutPanel();
            this.SuspendLayout();
            // 
            // question
            // 
            this.question.ColumnCount = 1;
            this.question.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.question.Dock = System.Windows.Forms.DockStyle.Fill;
            this.question.Location = new System.Drawing.Point(0, 0);
            this.question.Name = "question";
            this.question.RowCount = 2;
            this.question.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.question.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.question.Size = new System.Drawing.Size(561, 150);
            this.question.TabIndex = 0;
            this.question.Paint += new System.Windows.Forms.PaintEventHandler(this.question_Paint);
            // 
            // QuestionCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.question);
            this.Name = "QuestionCard";
            this.Size = new System.Drawing.Size(561, 150);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel question;
    }
}
