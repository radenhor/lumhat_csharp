﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Model;
using Lumhat.Configuration;
namespace WindowsFormsApp1.Service
{
    class CategoryService
    {
        public static NpgsqlConnection conn;
        public CategoryService()
        {
            //conn = new NpgsqlConnection("Server=52.77.227.48; Port=5432; User Id=rabbit; Password=rabbit+_); Database=lumhat_db");
            //conn.Open();
        }

        public List<SubMajor> findSubMajorByCategory(int majorID)
        {
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM lh_sub_major where major_id = "+majorID, DatabaseConnection.Instance.GetDBConnection());
            NpgsqlDataReader dr = cmd.ExecuteReader();
            List<SubMajor> categories = new List<SubMajor>();
            while (dr.Read())
            {
                categories.Add(new SubMajor(Convert.ToInt32(dr[0]), ""+dr[1]));
            }
            dr.Close();
            return categories;
        }

        public List<Subject> findSubjectBySubMajor(int subMajorID)
        {
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM lh_sub_major where parent_id = " + subMajorID, DatabaseConnection.Instance.GetDBConnection());
            NpgsqlDataReader dr = cmd.ExecuteReader();
            List<Subject> subjects = new List<Subject>();
            while (dr.Read())
            {
                subjects.Add(new Subject(Convert.ToInt32(dr[0]), "" + dr[1]));
            }
            dr.Close();
            return subjects;
        }
           
        public List<Level> findLevelBySubMajorId(int id)
        {
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT DISTINCT l.id, level FROM lh_level l INNER JOIN lh_quiz q ON l.id = q.level_id WHERE q.sub_major_id = "+id+" ORDER BY l.id", DatabaseConnection.Instance.GetDBConnection());
            NpgsqlDataReader dr = cmd.ExecuteReader();
            List<Level> levels = new List<Level>();
            while (dr.Read())
            {
                levels.Add(new Level(Convert.ToInt32(dr[0]), "" + dr[1]));
            }
            dr.Close();
            return levels;
        }
    }
}
