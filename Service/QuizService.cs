﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Service;
using WindowsFormsApp1.Model;
using Lumhat.Model;
using Lumhat.Configuration;
using Npgsql;

namespace WindowsFormsApp1.Service
{
    class QuizService
    {
        public List<QuizShow> findAllQuizByLevelAndSubMajor(int lID,int subID)
        {
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM lh_quiz where sub_major_id = " + subID + "and level_id = " + lID , DatabaseConnection.Instance.GetDBConnection());
            NpgsqlDataReader dr = cmd.ExecuteReader();
            List<QuizShow> quizzes = new List<QuizShow>();
            while (dr.Read())
            {
                quizzes.Add(new QuizShow(Convert.ToInt32(dr[0]),""+dr[1],Convert.ToInt32(dr[2])/60));
            }
            dr.Close();
            return quizzes;
        }

        public List<Instruction> findAllInstructionByQuiz(int quizId)
        {
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM lh_instruction where quiz_id = " + quizId , DatabaseConnection.Instance.GetDBConnection());
            NpgsqlDataReader dr = cmd.ExecuteReader();
            List<Instruction> instructions = new List<Instruction>();
            while (dr.Read())
            {
                instructions.Add(new Instruction(Convert.ToInt32(dr[0]), "" + dr[1]));
            }
            dr.Close();
            return instructions;
        }

        public List<Question> findAllQuestionByInstruction(int insId)
        {
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM lh_question where instruction_id = " + insId, DatabaseConnection.Instance.GetDBConnection());
            NpgsqlDataReader dr = cmd.ExecuteReader();
            List<Question> questions = new List<Question>();
            while (dr.Read())
            {
                questions.Add(new Question(Convert.ToInt32(dr[0]), "" + dr[1]));
            }
            dr.Close();
            return questions;
        }

        public List<Answer> findAllAnswerByQuestion(int qId)
        {
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM lh_answer where question_id = " + qId, DatabaseConnection.Instance.GetDBConnection());
            NpgsqlDataReader dr = cmd.ExecuteReader();
            List<Answer> answers = new List<Answer>();
            while (dr.Read())
            {
                answers.Add(new Answer(Convert.ToInt32(dr[0]), "" + dr[1], (bool)dr["iscorrect"]));
            }
            dr.Close();
            return answers;
        }
    }
}
