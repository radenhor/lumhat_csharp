﻿namespace WindowsFormsApp1
{
    partial class Lumhat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Lumhat));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.subPanel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.subPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.english = new System.Windows.Forms.PictureBox();
            this.korean = new System.Windows.Forms.PictureBox();
            this.program = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.catPanel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.subMajor = new System.Windows.Forms.TreeView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.level = new System.Windows.Forms.Panel();
            this.tabLevel = new System.Windows.Forms.TabControl();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.mainPanel.SuspendLayout();
            this.subPanel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.subPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.english)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.korean)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.program)).BeginInit();
            this.catPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.level.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(809, 450);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.Color.Transparent;
            this.mainPanel.Controls.Add(this.subPanel2);
            this.mainPanel.Controls.Add(this.subPanel1);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(809, 450);
            this.mainPanel.TabIndex = 2;
            this.mainPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // subPanel2
            // 
            this.subPanel2.BackColor = System.Drawing.Color.White;
            this.subPanel2.Controls.Add(this.tableLayoutPanel2);
            this.subPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.subPanel2.Location = new System.Drawing.Point(0, 267);
            this.subPanel2.Name = "subPanel2";
            this.subPanel2.Size = new System.Drawing.Size(809, 183);
            this.subPanel2.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(809, 183);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("PMingLiU-ExtB", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(803, 183);
            this.label4.TabIndex = 0;
            this.label4.Text = "Let Do Quiz!!!";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // subPanel1
            // 
            this.subPanel1.BackColor = System.Drawing.Color.Transparent;
            this.subPanel1.ColumnCount = 3;
            this.subPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.15871F));
            this.subPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.68258F));
            this.subPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.15872F));
            this.subPanel1.Controls.Add(this.english, 0, 0);
            this.subPanel1.Controls.Add(this.korean, 1, 0);
            this.subPanel1.Controls.Add(this.program, 2, 0);
            this.subPanel1.Controls.Add(this.label1, 0, 1);
            this.subPanel1.Controls.Add(this.label2, 1, 1);
            this.subPanel1.Controls.Add(this.label3, 2, 1);
            this.subPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.subPanel1.Location = new System.Drawing.Point(0, 0);
            this.subPanel1.Name = "subPanel1";
            this.subPanel1.Padding = new System.Windows.Forms.Padding(20, 20, 20, 0);
            this.subPanel1.RowCount = 2;
            this.subPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.subPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this.subPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.subPanel1.Size = new System.Drawing.Size(809, 267);
            this.subPanel1.TabIndex = 1;
            this.subPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // english
            // 
            this.english.Dock = System.Windows.Forms.DockStyle.Fill;
            this.english.Image = global::Lumhat.Properties.Resources.English;
            this.english.Location = new System.Drawing.Point(23, 23);
            this.english.Name = "english";
            this.english.Size = new System.Drawing.Size(248, 200);
            this.english.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.english.TabIndex = 0;
            this.english.TabStop = false;
            this.english.Click += new System.EventHandler(this.english_Click);
            // 
            // korean
            // 
            this.korean.Dock = System.Windows.Forms.DockStyle.Fill;
            this.korean.Image = global::Lumhat.Properties.Resources.korea;
            this.korean.Location = new System.Drawing.Point(277, 23);
            this.korean.Name = "korean";
            this.korean.Size = new System.Drawing.Size(253, 200);
            this.korean.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.korean.TabIndex = 1;
            this.korean.TabStop = false;
            this.korean.Click += new System.EventHandler(this.korean_Click);
            // 
            // program
            // 
            this.program.Dock = System.Windows.Forms.DockStyle.Fill;
            this.program.Image = global::Lumhat.Properties.Resources.IT;
            this.program.Location = new System.Drawing.Point(536, 23);
            this.program.Name = "program";
            this.program.Size = new System.Drawing.Size(250, 200);
            this.program.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.program.TabIndex = 2;
            this.program.TabStop = false;
            this.program.Click += new System.EventHandler(this.program_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(23, 226);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(248, 31);
            this.label1.TabIndex = 3;
            this.label1.Text = "ENGLISH";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(277, 226);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(253, 31);
            this.label2.TabIndex = 4;
            this.label2.Text = "KOREAN";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(536, 226);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(250, 31);
            this.label3.TabIndex = 5;
            this.label3.Text = "PROGRAMMING";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // catPanel
            // 
            this.catPanel.BackColor = System.Drawing.Color.SpringGreen;
            this.catPanel.Controls.Add(this.tableLayoutPanel1);
            this.catPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.catPanel.Location = new System.Drawing.Point(0, 0);
            this.catPanel.Name = "catPanel";
            this.catPanel.Size = new System.Drawing.Size(809, 450);
            this.catPanel.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Teal;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.subMajor, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(809, 450);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // subMajor
            // 
            this.subMajor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.subMajor.Location = new System.Drawing.Point(3, 3);
            this.subMajor.Name = "subMajor";
            this.subMajor.Size = new System.Drawing.Size(398, 411);
            this.subMajor.TabIndex = 0;
            this.subMajor.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.subMajor_AfterSelect);
            this.subMajor.Click += new System.EventHandler(this.subMajor_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.level, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(407, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.96396F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.03603F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(399, 411);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Comic Sans MS", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(393, 57);
            this.label5.TabIndex = 2;
            this.label5.Text = "តោះ! ធ្វើ​លំហាត់​ឥឡូវ​នេះ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // level
            // 
            this.level.Controls.Add(this.tabLevel);
            this.level.Dock = System.Windows.Forms.DockStyle.Fill;
            this.level.Location = new System.Drawing.Point(3, 60);
            this.level.Name = "level";
            this.level.Size = new System.Drawing.Size(393, 348);
            this.level.TabIndex = 3;
            // 
            // tabLevel
            // 
            this.tabLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabLevel.Location = new System.Drawing.Point(0, 0);
            this.tabLevel.Name = "tabLevel";
            this.tabLevel.SelectedIndex = 0;
            this.tabLevel.Size = new System.Drawing.Size(393, 348);
            this.tabLevel.TabIndex = 0;
            // 
            // button1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.button1, 2);
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(3, 420);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(803, 27);
            this.button1.TabIndex = 2;
            this.button1.Text = "Back to home";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Lumhat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(809, 450);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.catPanel);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Lumhat";
            this.Text = "Lumhat";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.mainPanel.ResumeLayout(false);
            this.subPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.subPanel1.ResumeLayout(false);
            this.subPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.english)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.korean)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.program)).EndInit();
            this.catPanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.level.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Panel subPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel subPanel1;
        private System.Windows.Forms.Panel catPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TreeView subMajor;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel level;
        private System.Windows.Forms.TabControl tabLevel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox english;
        private System.Windows.Forms.PictureBox korean;
        private System.Windows.Forms.PictureBox program;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

