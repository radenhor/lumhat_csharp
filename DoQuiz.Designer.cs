﻿namespace WindowsFormsApp1
{
    partial class DoQuiz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.duration = new System.Windows.Forms.Timer(this.components);
            this.panel = new System.Windows.Forms.Panel();
            this.root = new System.Windows.Forms.TableLayoutPanel();
            this.quiz = new System.Windows.Forms.TableLayoutPanel();
            this.quizPanel = new System.Windows.Forms.Panel();
            this.infoQuiz = new System.Windows.Forms.TableLayoutPanel();
            this.lblQuestionCount = new System.Windows.Forms.Label();
            this.lblDuration = new System.Windows.Forms.Label();
            this.btnDone = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.showResultPanel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblScore = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panelAnswer = new System.Windows.Forms.Panel();
            this.panel.SuspendLayout();
            this.root.SuspendLayout();
            this.quiz.SuspendLayout();
            this.infoQuiz.SuspendLayout();
            this.showResultPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.root);
            this.panel.Controls.Add(this.showResultPanel);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(777, 450);
            this.panel.TabIndex = 99;
            this.panel.Tag = "9999";
            // 
            // root
            // 
            this.root.BackColor = System.Drawing.Color.White;
            this.root.ColumnCount = 1;
            this.root.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.root.Controls.Add(this.quiz, 0, 0);
            this.root.Dock = System.Windows.Forms.DockStyle.Fill;
            this.root.Location = new System.Drawing.Point(0, 0);
            this.root.Name = "root";
            this.root.RowCount = 1;
            this.root.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.root.Size = new System.Drawing.Size(777, 450);
            this.root.TabIndex = 1;
            // 
            // quiz
            // 
            this.quiz.ColumnCount = 1;
            this.quiz.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.quiz.Controls.Add(this.quizPanel, 0, 0);
            this.quiz.Controls.Add(this.infoQuiz, 0, 1);
            this.quiz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.quiz.Location = new System.Drawing.Point(3, 3);
            this.quiz.Name = "quiz";
            this.quiz.RowCount = 2;
            this.quiz.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.63063F));
            this.quiz.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.36937F));
            this.quiz.Size = new System.Drawing.Size(771, 444);
            this.quiz.TabIndex = 0;
            // 
            // quizPanel
            // 
            this.quizPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.quizPanel.Location = new System.Drawing.Point(3, 3);
            this.quizPanel.Name = "quizPanel";
            this.quizPanel.Size = new System.Drawing.Size(765, 351);
            this.quizPanel.TabIndex = 2;
            this.quizPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.quizPanel_Paint);
            // 
            // infoQuiz
            // 
            this.infoQuiz.BackColor = System.Drawing.Color.Teal;
            this.infoQuiz.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.infoQuiz.ColumnCount = 2;
            this.infoQuiz.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.infoQuiz.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.infoQuiz.Controls.Add(this.lblQuestionCount, 0, 0);
            this.infoQuiz.Controls.Add(this.lblDuration, 1, 0);
            this.infoQuiz.Controls.Add(this.btnDone, 0, 1);
            this.infoQuiz.Controls.Add(this.btnBack, 1, 1);
            this.infoQuiz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoQuiz.Location = new System.Drawing.Point(3, 360);
            this.infoQuiz.Name = "infoQuiz";
            this.infoQuiz.RowCount = 2;
            this.infoQuiz.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.infoQuiz.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.infoQuiz.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.infoQuiz.Size = new System.Drawing.Size(765, 81);
            this.infoQuiz.TabIndex = 1;
            // 
            // lblQuestionCount
            // 
            this.lblQuestionCount.AutoSize = true;
            this.lblQuestionCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblQuestionCount.Font = new System.Drawing.Font("MS Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuestionCount.ForeColor = System.Drawing.Color.White;
            this.lblQuestionCount.Location = new System.Drawing.Point(5, 2);
            this.lblQuestionCount.Name = "lblQuestionCount";
            this.lblQuestionCount.Size = new System.Drawing.Size(373, 42);
            this.lblQuestionCount.TabIndex = 0;
            this.lblQuestionCount.Text = "Question 0/200";
            this.lblQuestionCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDuration
            // 
            this.lblDuration.AutoSize = true;
            this.lblDuration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDuration.Font = new System.Drawing.Font("MS Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDuration.ForeColor = System.Drawing.Color.White;
            this.lblDuration.Location = new System.Drawing.Point(386, 2);
            this.lblDuration.Name = "lblDuration";
            this.lblDuration.Size = new System.Drawing.Size(374, 42);
            this.lblDuration.TabIndex = 1;
            this.lblDuration.Text = "10:00";
            this.lblDuration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDone
            // 
            this.btnDone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDone.Location = new System.Drawing.Point(5, 49);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(373, 27);
            this.btnDone.TabIndex = 2;
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBack.Location = new System.Drawing.Point(386, 49);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(374, 27);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "Back to home";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // showResultPanel
            // 
            this.showResultPanel.BackColor = System.Drawing.Color.Teal;
            this.showResultPanel.Controls.Add(this.tableLayoutPanel1);
            this.showResultPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.showResultPanel.Location = new System.Drawing.Point(0, 0);
            this.showResultPanel.Name = "showResultPanel";
            this.showResultPanel.Size = new System.Drawing.Size(777, 450);
            this.showResultPanel.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lblScore, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panelAnswer, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.79679F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.44444F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.777778F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(777, 450);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblScore.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScore.ForeColor = System.Drawing.Color.White;
            this.lblScore.Location = new System.Drawing.Point(3, 0);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(771, 107);
            this.lblScore.TabIndex = 0;
            this.lblScore.Text = "You got score 2/20 ";
            this.lblScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(3, 408);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(771, 39);
            this.button1.TabIndex = 1;
            this.button1.Text = "Back to home";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panelAnswer
            // 
            this.panelAnswer.BackColor = System.Drawing.Color.White;
            this.panelAnswer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelAnswer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAnswer.Location = new System.Drawing.Point(3, 110);
            this.panelAnswer.Name = "panelAnswer";
            this.panelAnswer.Size = new System.Drawing.Size(771, 292);
            this.panelAnswer.TabIndex = 2;
            // 
            // DoQuiz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 450);
            this.Controls.Add(this.panel);
            this.Name = "DoQuiz";
            this.Text = "DoQuiz";
            this.Load += new System.EventHandler(this.DoQuiz_Load);
            this.panel.ResumeLayout(false);
            this.root.ResumeLayout(false);
            this.quiz.ResumeLayout(false);
            this.infoQuiz.ResumeLayout(false);
            this.infoQuiz.PerformLayout();
            this.showResultPanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer duration;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.TableLayoutPanel root;
        private System.Windows.Forms.TableLayoutPanel quiz;
        private System.Windows.Forms.TableLayoutPanel infoQuiz;
        private System.Windows.Forms.Label lblQuestionCount;
        private System.Windows.Forms.Label lblDuration;
        private System.Windows.Forms.Panel quizPanel;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Panel showResultPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblScore;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panelAnswer;
    }
}